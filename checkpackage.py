#
#    Copyright (C) 2016 mtsio <mtsio@cryptolab.net>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

from distlib import locators
import sys

def check_package(package):
    is_free = False
    free_licenses = ['Academic Free License (AFL)', 'Apache Software License',
                     'Artistic License','Attribution Assurance License',
                     'BSD License',
                     'CEA CNRS Inria Logiciel Libre License, version 2.1'
                     '(CeCILL-2.1)', 'Common Public License',
                     'Eiffel Forum License',
                     'European Union Public License 1.0 (EUPL 1.0)',
                     'European Union Public License 1.1 (EUPL 1.1)',
                     'GNU Affero General Public License v3',
                     'GNU Affero General Public License v3 or later (AGPLv3+)',
                     'GNU Free Documentation License (FDL)',
                     'GNU General Public License (GPL)',
                     'GNU General Public License v2 (GPLv2)',
                     'GNU General Public License v2 or later (GPLv2+)',
                     'GNU General Public License v3 (GPLv3)',
                     'GNU General Public License v3 or later (GPLv3+)',
                     'GNU Lesser General Public License v2 (LGPLv2)',
                     'GNU Lesser General Public License v2 or later (LGPLv2+)',
                     'GNU Lesser General Public License v3 (LGPLv3)',
                     'GNU Lesser General Public License v3 or later (LGPLv3+)',
                     'GNU Library or Lesser General Public License (LGPL)',
                     'IBM Public License', 'ISC License (ISCL)', 'MIT License',
                     'Mozilla Public License 1.0 (MPL)',
                     'Mozilla Public License 1.1 (MPL 1.1)',
                     'Mozilla Public License 2.0 (MPL 2.0)',
                     'Open Group Test Suite License',
                     'Python License (CNRI Python License)',
                     'Python Software Foundation License', 'Sleepycat License',
                     'Sun Public License',
                     'University of Illinois/NCSA Open Source License',
                     'W3C License', 'Zope Public License',
                     'zlib/libpng License',
                     'CC0 1.0 Universal (CC0 1.0) Public Domain Dedication']
    
    locator = locators.PyPIJSONLocator('https://pypi.python.org/pypi/')
    info = locator._get_project(package)
    if not info:
        return False

    version = info.keys()[0]
    license = info[version].metadata.license
    classifiers = info[version].metadata.classifiers
    #print classifiers
    # Check categories for a free license
    osi_approved = 'License :: OSI Approved :: '
    for cl in classifiers:
        for fl in free_licenses:
            if cl == osi_approved + fl or cl == 'License :: ' + fl:
                if fl == 'Artistic License':
                    is_free = check_artistic_version(license)
                else:
                    is_free = True
                print cl

    return is_free

def check_artistic_version(license):
    print license
    if '2' in license:
        return True

    return False
    
package = sys.argv[1]
print (check_package(package))
